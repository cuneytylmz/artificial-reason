﻿$('#dropdownMenu1').on('show.bs.dropdown', function () {
    $('#dropdownMenu1').dropdown('toggle');
    $('body').animate()
})

$(document).ready(function () {
    $('.bxslider').bxSlider();
});

$('.bxslider').bxSlider({
    nextSelector: '#bx-ileri',
    prevSelector: '#bx-geri',
    nextText: '>',
    prevText: '<',
    minSlides: 4,
    maxSlides: 4,
    slideWidth: 275,
    pager: false
});